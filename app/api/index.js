const APIMessage = require('./message');
const Helper = require('./helper');

module.exports = (app, lib) => {
  let API = {
    APIMessage: APIMessage,
    ObjectID: require('mongodb').ObjectID,
    util: require('util'),
    md5: require('crypto-md5'),
    email: require('emailjs'),
    emailTemplate: require('./mailTemplate'),
    shortid: require('shortid'),
    randomItem: require('random-item'),
    hashObject: require('hash-object'),
    multer: require('multer'),
    fs: require('fs'),
    request: require('request'),
    stripTags: require('striptags'),
    performance: require('performance-now'),
    base64Image: require('base64-img')
  };
  let email = require('emailjs');
  let server = email.server.connect({user: 'gemofilik-noreply@mail.ru', password: '25olroma1998', host: 'smtp.mail.ru', ssl: true, port: 465});
  lib = Object.assign(lib, API);
  lib = Object.assign(lib, {helper: new Helper(lib), email: server});
  require('./routes')(app, lib);
};
