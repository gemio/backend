class Doctor {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для отправки запроса на добавление в лечащего врача
   * */
  addTherapist(request, response) {
    this.lib.log.trace('Method "addTherapist" - вызван');
    if (!request.params.token || !request.body.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addTherapist" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      if (user.role.name === 'Специалист-Консултант') {
        response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_WRONG_ROLE());
        return;
      }

      this.helper.searchUserByID(request.body.userID).then(userTherapist => {
        if (userTherapist.id === user.id) {
          response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_ADD_SAME_USER());
          return;
        } else if (userTherapist.role.name !== 'Специалист-консультант') {
          response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_ADD_SIMPLE_USER());
          return;
        }

        const userToken = this.lib.ObjectID(user._id), specialistToken = this.lib.ObjectID(userTherapist._id);
        this.lib.db.collection('friends').findOne({$and: [{user1: userToken}, {user2: specialistToken}, {status: 'accepted'}]}, (err, item) => {
          if (err)response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else if (item === null) response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_ADD_WITHOUT_FRIEND_ACCEPT());
          else {
            let allTherapistUser = userTherapist.therapist || [];
            for (let i = 0; i < allTherapistUser.length; i++) {
              if (allTherapistUser[i].userID === user.id) {
                response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_ADD_USER_THAT_ALREADY_ADDED());
                return;
              }
            }

            if (!Array.isArray(allTherapistUser)) allTherapistUser = [];

            allTherapistUser.push({
              userID: user.id,
              status: 'pending'
            });

            // update therapist users on specialist account first
            this.lib.db.collection('users').updateOne({id: request.body.userID}, {$set: {therapist: allTherapistUser}}, (err, item) => {
              if (err)response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else {
                // Then update a user, that send this request
                this.lib.db.collection('users').updateOne({id: user.id}, {$set: {therapist: {userID: userTherapist.id, status: 'pending'}}}, (err, item) => {
                  if (err)response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
                  else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
                });
              }
            });
          }
        });
      }).catch(err => {
        response.status(err.status).send(err.message);
      })
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для принятия запроса пользователя на добавления врача в качестве лечащего
   * */
  acceptTherapist(request, response) {
    this.lib.log.trace('Method "acceptTherapist" - вызван');
    if (!request.params.token || !request.body.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "acceptTherapist" - прошел валидацию параметров');
    this.helper.searchUserByTokenAndValidateSpecialistRights(request.params.token).then(user => {
      let usersTherapistIndex = user.therapist.findIndex(therapist => therapist.userID === request.body.userID && therapist.status === 'pending');

      // If we not found a user in therapist list
      // Then send an error
      if (usersTherapistIndex === -1) {
        response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_FOUND_USER_IN_LIST());
      } else {
        // Else we just need to update status and update an array in database
        user.therapist[usersTherapistIndex].status = 'accepted';
        this.lib.db.collection('users').updateOne({id: user.id}, {$set: {therapist: user.therapist}}, (err, item) => {
          if (err)response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else {
            // Than we need update an user, that send a request for add to therapist
            const therapist = {
              userID: user.id,
              status: 'accepted'
            };

            this.lib.db.collection('users').updateOne({id: user.therapist[usersTherapistIndex].userID}, {$set: {therapist: therapist}}, (err, item) => {
              if (err)response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
            });
          }
        });
      }
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения информации о лечащем враче по статусу
   * */
  getTherapistByStatus(request, response) {
    this.lib.log.trace('Method "getTherapistByStatus" - вызван');
    if (!request.params.token || !request.query.status || (request.query.status !== 'pending' && request.query.status !== 'accepted')) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getTherapistByStatus" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      let therapistPendingStatus = user.role.name === 'Специалист-консультант' ?
        user.therapist.filter(therapist => therapist.status === request.query.status) : user.therapist;
      let waiting = [];
      if (Array.isArray(therapistPendingStatus)) {
        for (let i = 0; i < therapistPendingStatus.length; i++) {
          waiting.push(new Promise((resolve, reject) => {
            this.lib.db.collection('users').findOne({id: therapistPendingStatus[i].userID}, (err, item) => {
              if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
              else if (item === null) resolve();
              else resolve({
                  userID: item.id,
                  fullName: this.helper.getShortFullName(item.public.fullName),
                  avatar: this.helper.getUserAvatarByID(item.id),
                });
            });
          }));
        }
      } else {
        if (therapistPendingStatus.status !== request.query.status) therapistPendingStatus = undefined;

        if (!therapistPendingStatus) {
          response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
          return;
        }

        waiting.push(new Promise((resolve, reject) => {
          this.lib.db.collection('users').findOne({id: therapistPendingStatus.userID}, (err, item) => {
            if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
            else if (item === null) resolve();
            else resolve({
                userID: item.id,
                fullName: this.helper.getShortFullName(item.public.fullName),
                avatar: this.helper.getUserAvatarByID(item.id)
              });
          });
        }));
      }

      Promise.all(waiting).then(data => {
        response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(data));
      }).catch(err => {
        response.status(err.status || 500).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения статуса доктора только для пользователя
   * */
  getUserStatusTherapist(request, response) {
    this.lib.log.trace('Method "getUserStatusTherapist" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getUserStatusTherapist" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      if (user.role.name === 'Специалист-консультант') {
        response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
        return;
      }

      response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({status: user.therapist.status || 'not'}));
    }).catch(err => {
      response.status(err.status || 500).send(err.message);
    });
  }

  /**
   * Метод для получения информации о всех врачах
   * */
  getListOfDoctors(request, response) {
    this.lib.log.trace('Method "getListOfDoctors" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.helper.searchUserByToken(request.params.token).then(user => {
      let query = [
        {$lookup: {
          from: 'friends',
          localField: '_id',
          foreignField: 'user1',
          as: 'isFriends'
        }}
      ];

      this.lib.db.collection('users').aggregate(query).toArray((err, users) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else {
          users = users.filter(user => user.role.name === 'Специалист-консультант');
          for (let i = 0; i < users.length; i++) {
            for (let j = 0; j < users[i].isFriends.length; j++) {
              if (users[i].isFriends[j].user1.toString() === request.params.token || users[i].isFriends[j].user2.toString() === request.params.token && users[i].isFriends[j].status === 'accepted')
                users[i].isFriends = true;
            }

            if (Array.isArray(users[i].isFriends))
              users[i].isFriends = false;
          }

          users = users.map(user => {
            return Object.assign({}, this.helper.getBasicUserInfo(user), {isFriends: user.isFriends})
          });

          response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для отправки сообщения
   * */
  sendMessage(request, response) {
    this.lib.log.trace('Method "sendMessage" - вызван');
    if (!request.params.token || !request.query.userID || !request.body.text) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "sendMessage" - прошел валидацию параметров');

    request.body.text = this.lib.stripTags(request.body.text);
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.helper.searchUserByID(request.query.userID).then(queryUser => {
        let therapist = user.therapist;

        // If our therapist information is array (user is specialist)
        // Than we need to check user is accepted or in array
        if (Array.isArray(therapist)) {
          let index = therapist.findIndex(userTherapist => userTherapist.userID === request.query.userID && userTherapist.status === 'accepted');
          if (index === -1) {
            response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_SEND_MESSAGE_NO_ACCEPTED());
            return;
          } else {
            // Else we have an user with status "accepted" in array
            therapist = therapist[index];
          }
        } else {
          if (therapist.userID !== request.query.userID) {
            response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_SEND_MESSAGE_NO_ACCEPTED());
            return;
          }
        }

        const message = {
          userIDFrom: user.id,
          userIDTo: therapist.userID,
          userFullNameFrom: this.helper.getShortFullName(user.public.fullName),
          userFullNameTo: this.helper.getShortFullName(queryUser.public.fullName),
          date: new Date(),
          text: request.body.text
        };

        this.lib.db.collection('doctor-messages').insertOne(message, (err, item) => {
          if (err)response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
        });
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения сообщений
   * */
  getMessages(request, response) {
    this.lib.log.trace('Method "getMessages" - вызван');
    if (!request.params.token || !request.query.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getMessages" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      let therapist = user.therapist;
      // If our therapist information is array (user is specialist)
      // Than we need to check user is accepted or in array
      if (Array.isArray(therapist)) {
        let index = therapist.findIndex(userTherapist => userTherapist.userID === request.query.userID && userTherapist.status === 'accepted');
        if (index === -1) {
          response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_SEND_MESSAGE_NO_ACCEPTED());
          return;
        }
      } else {
        if (therapist.userID !== request.query.userID) {
          response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_SEND_MESSAGE_NO_ACCEPTED());
          return;
        }
      }

      let query = [
        {$match: {$or: [{userIDFrom: user.id, userIDTo: request.query.userID}, {userIDFrom: request.query.userID, userIDTo: user.id}]}}
      ];
      this.lib.db.collection('doctor-messages').aggregate(query).sort({'date': 1}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(204).send();
        else {
          for (let i = 0; i < item.length; i++) {
            item[i].userFrom = {
              id: item[i].userIDFrom,
              fullName: item[i].userFullNameFrom,
              avatar: this.helper.getUserAvatarByID(item[i].userIDFrom)
            };

            item[i].userTo = {
              id: item[i].userIDTo,
              fullName: item[i].userFullNameTo,
              avatar: this.helper.getUserAvatarByID(item[i].userIDTo)
            };

            item[i].userIDTo = undefined;
            item[i].userIDFrom = undefined;

            item[i].userFullNameTo = undefined;
            item[i].userFullNameFrom = undefined;
          }

          response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для добавления нового плана для пользовтаеля
   * */
  addRemindToUser(request, response) {
    const validateDate = (date) => {
      return new Date(date) != 'Invalid Date';
    };

    this.lib.log.trace('Method "addRemindToUser" - вызван');
    if (!request.params.token || !request.query.userID || !request.body.remind) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addRemindToUser" - прошел валидацию параметров');
    const remind = this.helper.tryParseJson(this.lib.stripTags(request.body.remind));
    if (!remind || !remind.type || !remind.rangeDate || !remind.title || !remind.description) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.helper.searchUserByTokenAndValidateSpecialistRights(request.params.token).then(user => {
      this.helper.searchUserByID(request.query.userID).then(userQuery => {
        let index = user.therapist.findIndex(userTherapist => userTherapist.userID === userQuery.id);

        if (index === -1) {
          response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_FOUND_USER_IN_LIST());
          return;
        }

        // validating remind date
        let date = remind.rangeDate.split('IO');
        let dateFormatCheck = validateDate(date[0]) && validateDate(date[1]);
        if (date.length < 2 || !dateFormatCheck) {
          response.status(400).send(this.lib.APIMessage.ERROR.WRONG_DATE_FORMAT());
          return;
        }

        date = {from: new Date(date[0]), to: new Date(date[1])};

        const remindAdded = {
          type: remind.type,
          isPlan: true,
          title: remind.title,
          startDate : date.from.toISOString(),
          endDate : date.to.toISOString(),
          desc: remind.description
        };

        userQuery.reminds.push(remindAdded);
        this.lib.db.collection('users').updateOne({id: userQuery.id}, {$set: {reminds: userQuery.reminds}}, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
        });
      }).catch(err => {
        response.status(err.status || 500).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения напомнинаний-плана пользовтаеля
   * */
  getRemindsOfUser(request, response) {
    this.lib.log.trace('Method "getRemindsOfUser" - вызван');
    if (!request.params.token || !request.query.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getRemindsOfUser" - прошел валидацию параметров');
    this.helper.searchUserByTokenAndValidateSpecialistRights(request.params.token).then(user => {
      this.helper.searchUserByID(request.query.userID).then(queryUser => {
        if (user.therapist.findIndex(userTherapist => userTherapist.userID === queryUser.id) === -1) {
          response.status(412).send(this.lib.APIMessage.ERROR.THERAPIST_CANT_FOUND_USER_IN_LIST());
          return;
        }

        response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(queryUser.reminds.filter(remind => remind.isPlan)))
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = Doctor;
