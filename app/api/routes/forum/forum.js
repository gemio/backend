class Forum {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
    this.getIdsFromRoot = (root) => {
      const getCountOfThread = (id) => {
        return new Promise((resolve, reject) => {
          this.lib.db.collection('forum').find({themeId: id}).toArray((err, item) => {
            if (err) reject({status: 500, message: err});
            else resolve({id: id, count: item.length});
          })
        });
      };

      let ids = [];
      this.helper.deepForEach(root, (value, key, subject, path) => {
        if (key === 'id') ids.push(value);
      });

      let waiting = [];
      for (let id of ids) {
        waiting.push(getCountOfThread(id));
      }

      return new Promise((resolve, reject) => {
        Promise.all(waiting).then(values => {
          resolve(values);
        }).catch(err => {
          reject(err);
        })
      });
    };

    this.getIds = () => {
      return new Promise((resolve, reject) => {
        this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
          if (err) reject({status: 500, message: err});
          else {
            let ids = [];
            for (let i = 0; i < item.length; i++) {
              ids.push([]);
              this.helper.deepForEach(item[i], (value, key, subject, path) => {
                if (key === 'id') ids[i].push(value);
              });
              resolve({ids: ids, structure: item});
            }
          }
        });
      });
    };

    this.validateThemeId = (id, isNeedNameOfTheme = false) => {
      return new Promise((resolve, reject) => {
        this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
          if (err) reject({status: 500, message: err});
          else {
            let saveId;
            for (let i = 0; i < item.length; i++) {
              this.helper.deepForEach(item[i], (value, key, subject, path) => {
                if (key === 'id' && value === id) {
                  if (!isNeedNameOfTheme) resolve({root: item[i], subject: subject});
                  else saveId = value;
                }else if (isNeedNameOfTheme && saveId && key === 'root') resolve({root: item[i], themeName: value, id: saveId, subject: subject});
              });
            }
            reject({status: 204, message: this.lib.APIMessage.ERROR.WRONG_FORUM_THEME_ID()});
          }
        });
      });
    }
  }

  /**
   * Метод для добавления нового топика в тему
   * */
  addTopic(request, response) {
    this.lib.log.trace('Method addTopic - вызван');
    if (!request.params.token || !request.body.text || !request.body.themeId || !request.body.title) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method addTopic - прошел валидацию параметров');
    request.body.text = this.lib.stripTags(request.body.text);
    request.body.title = this.lib.stripTags(request.body.title);
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.validateThemeId(request.body.themeId).then(themeData => {
        const topic = {
          themeId: request.body.themeId,
          themeRootId: themeData.root.id,
          owner: {
            fullName: this.helper.getShortFullName(user.public.fullName),
            id: user.id
          },
          text: request.body.text,
          title: request.body.title,
          comments: [],
          dateCreated: new Date().toISOString()
        };

        this.lib.db.collection('forum').insertOne(topic, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({id: item.ops[0]._id}));
        })
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  /**
   * Метод для добавления комментария к топику по его id
   * */
  addCommentTopic(request, response) {
    this.lib.log.trace('Method addCommentTopic - вызван');
    if (!request.query.topicId || !request.body.comment || !request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method addCommentTopic - прошел валидацию параметров');
    const userComment = this.lib.stripTags(request.body.comment);
    this.helper.searchUserByToken(request.params.token).then(user => {
      const comment = {
        user: {
          userID: user.id,
          fullName: this.helper.getShortFullName(user.public.fullName)
        },
        text: userComment,
        date: new Date().toISOString()
      };

      this.lib.db.collection('forum').updateOne({_id: this.lib.ObjectID(request.query.topicId)}, {$addToSet: {comments: comment}}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.matchedCount === 0) response.status(406).send(this.lib.APIMessage.ERROR.WRONG_FORUM_TOPIC_ID());
        else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
      })
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  /**
   * Метод для получения записей в теме по его id
   * */
  getTopicsFromThemeId(request, response) {
    this.lib.log.trace('Method getTopicsFromThemeId - вызван');
    if (!request.query.themeId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method getTopicsFromThemeId - прошел валидацию параметров');

    this.lib.db.collection('forum').find({themeId: request.query.themeId}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else {
        for (let i = 0; i < item.length; i++) {
          item[i].owner.avatar = this.helper.getUserAvatarByID(item[i].owner.id);
          if (item[i].comments.length > 0) {
            item[i].comments = {
              last: item[i].comments[item[i].comments.length - 1],
              count: item[i].comments.length
            };
            item[i].comments.last.user.avatar = this.helper.getUserAvatarByID(item[i].comments.last.user.userID);
          } else {
            item[i].comments = {
              count: 0
            }
          }
        }
        let topics = item;
        this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else {
            let root;
            for (let i = 0; i < item.length; i++) {
              this.helper.deepForEach(item[i], (value, key, subject, path) => {
                if (key === 'id' && value === request.query.themeId) root = subject;
              });
            }

            // if we have a subtheme
            // than we need to get count of topics in this themes
            if (root && root.subroot) {
              let waiting = [];
              for (let i = 0; i < root.subroot.length; i++) {
                waiting.push(new Promise((resolve, reject) => {
                  this.lib.db.collection('forum').find({themeId: root.subroot[i].id}).toArray((err, item) => {
                    if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                    else {
                      root.subroot[i].count = item.length;
                      resolve(root.subroot[i]);
                    }
                  });
                }));
              }

              Promise.all(waiting).then(values => {
                response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(Object.assign({}, {topics: topics.length > 0 ? topics : [], subtheme: values})));
              }).catch(err => {
                response.status(err.status).send(err.message);
              })
            } else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(Object.assign({}, {topics: topics.length > 0 ? topics : [], subtheme: []})));
          }
        });
      }
    });
  }

  /**
   * Метод для получения данных о топике
   * */
  getTopicDetails(request, response) {
    this.lib.log.trace('Method getTopicDetails - вызван');
    if (!request.query.topicId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method getTopicDetails - прошел валидацию параметров');
    this.lib.db.collection('forum').findOne({_id: this.lib.ObjectID(request.query.topicId)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(406).send(this.lib.APIMessage.ERROR.WRONG_FORUM_TOPIC_ID());
      else {
        item.owner.avatar = this.helper.getUserAvatarByID(item.owner.id);
        for (let i = 0; i < item.comments.length; i++)
          item.comments[i].user.avatar = this.helper.getUserAvatarByID(item.comments[i].user.userID);
        this.validateThemeId(item.themeId, true).then(rootData => {
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(Object.assign({}, item, {themeName: rootData.themeName})));
        }).catch(err => {
          response.status(err.status).send(err.message);
        });
      }
    });
  }

  /**
   * Метод для получения мини структуры главной темы по названию
   * */
  getMiniStructureOfRootTheme(request, response) {
    this.lib.log.trace('Method getMiniStructureOfRootTheme - вызван');
    if (request.query.themeId) {
      this.validateThemeId(request.query.themeId).then(rootData => {
        const rootName = rootData.root.root;
        this.lib.db.collection('forum-structure').findOne({root: rootName}, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else if (item === null) response.status(204).send();
          else {
            let ids = [], j = 0, indexCurrent = -1;
            this.helper.deepForEach(item, (value, key, subject, path) => {
              if (key === 'id') {
                ids.push({id: value});
                if (value === request.query.themeId) {
                  indexCurrent = j;
                  ids[j].isCurrent = true;
                }
              }
              else if (key === 'root') {
                ids[j].root = value;
                j++;
              }
            });

            // filter ids by containing a char which represent a full path
            ids.splice(indexCurrent + 1, ids.length);
            let idIndexesToDelete = [];
            for (let i = ids.length - 1; i >= 1; i--) {
              if (ids[i].id[1] !== ids[indexCurrent].id[1]) idIndexesToDelete.push(i);
            }

            for (let i = 0; i < idIndexesToDelete.length; i++)
              ids.splice(idIndexesToDelete[i], 1);


            response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(ids));
          }
        });
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    } else {
      this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(204).send();
        else {
          let ids = [], j = 0;
          this.helper.deepForEach(item, (value, key, subject, path) => {
            if (key === 'id') {
              ids.push({id: value});
              if (value === request.query.themeId) {
                ids[j].isCurrent = true;
              }
            }
            else if (key === 'root') {
              ids[j].root = value;
              j++;
            }
          });

          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(ids));
        }
      });
    }
  }

  /**
   * Метод для получения кол-ва созданных топиков у пользователя
   * */
  getUserCountThemes(request, response) {
    this.lib.log.trace('Method getUserCountThemes - вызван');
    if (!request.params.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method getUserCountThemes - прошел валидацию параметров');
    this.helper.searchUserByID(request.params.userID).then(user => {
      this.lib.db.collection('forum').find({"owner.id": request.params.userID}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({countThemes: item.length}));
      })
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения детальной информации о теме
   * */
  getThemeDetails(request, response) {
    this.lib.log.trace('Method getThemeDetails - вызван');
    if (!request.query.themeId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method getThemeDetails - прошел валидацию параметров');
    this.validateThemeId(request.query.themeId, true).then(rootData => {
      rootData.root.subroot = undefined;
      rootData.subtheme = rootData.subject;
      rootData.subject = undefined;
      response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(rootData));
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения части комментов топика по id
   * */
  getCommentsOfTopicByOffset(request, response) {
    this.lib.log.trace('Method getCommentsOfTopicByOffset - вызван');
    if (!request.query.topicId || !request.query.offset || !request.query.count || request.query.count < 0 || request.query.offset < 0) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method getCommentsOfTopicByOffset - прошел валидацию параметров');
    this.lib.db.collection('forum').findOne({_id: this.lib.ObjectID(request.query.topicId)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(406).send(this.lib.APIMessage.ERROR.WRONG_FORUM_TOPIC_ID());
      else {
        if (!Number.isInteger(+request.query.offset) || !Number.isInteger(+request.query.count) || +request.query.offset > item.comments.length) {
          response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
          return;
        }

        const offset = +request.query.offset, needCount = +request.query.count;
        let comments = [], count = 0;
        for (let i = offset; i < item.comments.length; i++) {
          if (count === needCount) break;
          comments.push(item.comments[i]);
          comments[comments.length - 1].user.avatar = this.helper.getUserAvatarByID(comments[comments.length - 1].user.userID);
          count++;
        }

        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({count: item.comments.length, comments: comments}));
      }
    });
  }

  /**
   * Метод для получения полной структуры форума
   * */
  getFullStructureForum(request, response) {
    this.lib.log.trace('Method getFullStructureForum - вызван');
    let timer = this.lib.performance();
    this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204).send();
      else {
        let waiting = [];
        for (let root of item) waiting.push(this.getIdsFromRoot(root));

        Promise.all(waiting).then(values => {
          let allSum = [];
          for (let i = 0; i < values.length; i++) {
            let sum = 0, savedSumBySecondChar = [];
            for (let j = values[i].length - 1; j >= 0; j--) {
              let saveCount = values[i][j].count;
              values[i][j].count += sum;
              if (sum !== values[i][j].count) {
                let index = savedSumBySecondChar.findIndex(sum => sum.alpha === values[i][j].id[1]);

                // if we not have saved count in array
                if (index === -1) {
                  savedSumBySecondChar.push({alpha: values[i][j].id[1], sum: saveCount});
                } else {
                  // if we have a saved count by second char
                  // then we need only increment this char
                  savedSumBySecondChar[index].sum += saveCount;
                }

                sum += saveCount;
              }
            }
            allSum.push(savedSumBySecondChar);
          }

          for (let i = 0; i < values.length; i++) {
            let saveValues = values[i];
            values[i] = values[i].filter(value => value.id.length === 3);

            for (let j = 0; j < values[i].length; j++) {
              for (let k = 0; k < allSum[i].length; k++) {
                let index = allSum[i].findIndex(sum => sum.alpha === values[i][j].id.charAt(1));
                if (index !== -1) {
                  values[i][j].count = allSum[i][index].sum;
                }
              }
            }

            if (values[i].length === 0) {
              values[i] = [{
                id: saveValues[0].id,
                count: saveValues[0].count
              }];
            }
          }

          this.lib.log.trace(`Method getFullStructureForum - время на парс структуры: ${(timer - this.lib.performance()).toFixed(3)} мс`);
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(Object.assign({}, {structure: item}, {counts: values})));
        }).catch(err => {
          response.status(err.status).send(err.message);
        })
      }
    });
  }

  /**
   * Метод для добавления глобальной темы к структуре форума
   * */
  addGlobalThemeToStructure(request, response) {
    this.lib.log.trace('Method addGlobalThemeToStructure - вызван');
    if (!request.params.token || !request.body.title) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method addGlobalThemeToStructure - прошел валидацию параметров');
    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      // Get current forum structure
      this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else {
          const GLOBAL_STRUCTURE_CURRENT = item, title = this.lib.stripTags(request.body.title);
          let newGlobalTheme = {
            id: '',
            root: title,
            subroot: []
          };
          let indexOfChar = GLOBAL_STRUCTURE_CURRENT.slice().reverse().findIndex(global => global.root.charAt(0) === title.charAt(0));
          if (indexOfChar !== -1) indexOfChar = (GLOBAL_STRUCTURE_CURRENT.length - 1) - indexOfChar;

          // If we have theme with first char that equals first char in our title
          // Than we need increment a number in this id
          if (indexOfChar !== -1) {
            let idTemp = GLOBAL_STRUCTURE_CURRENT[indexOfChar].id.replace( /[^\d.]/g, '' );
            newGlobalTheme.id = `${title.charAt(0)}${+idTemp + 1}`
          } else {
            // If we have not exist a char than just simply start with char at 1 index and 1 counter
            // EXAMPLE: Образование -> О1
            newGlobalTheme.id = `${title.charAt(0)}1`;
          }

          this.lib.db.collection('forum-structure').insertOne(newGlobalTheme, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
            else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
          });
        }
      });
    }).catch(err =>{
     response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для добавления подраздолов в теме
   * */
  addSubthemeToForum(request, response) {
    const generateId = (clearId) => {
      return new Promise((resolve, reject) => {
        this.getIds().then(idsData => {
          let idInt = 1, id = `${clearId}${idInt}`, generated = false, timer = this.lib.performance();

          while (!generated) {
            let founded = false;

            for (let i = 0; i < idsData.ids.length; i++) {
              let index = idsData.ids[i].findIndex(ids => ids === id);
              if (index !== -1) {
                founded = true;
                break;
              }
            }

            if (!founded) generated = true;
            else id = `${clearId}${++idInt}`;
          }

          this.lib.log.trace(`Method addSubthemeToForum - id был сгенерирован за ${(timer - this.lib.performance()).toFixed(3)} мс`);
          resolve(id);
        }).catch(err => {
          reject(err);
        })
      });
    };

    this.lib.log.trace('Method addSubthemeToForum - вызван');
    if (!request.params.token || !request.body.name || !request.body.parentInfo) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method addSubthemeToForum - прошел валидацию параметров');

    // parent.index (array) -> 0 - index of global theme, 1 - index of sub theme of global, 2 - index of sub theme of sub theme
    const parent = this.helper.tryParseJson(this.lib.stripTags(request.body.parentInfo));
    parent.children = parent.children || [];
    const themeInfo = {
      name: this.lib.stripTags(request.body.name)
    };

    if (!parent || !parent.index || !Array.isArray(parent.index) || parent.index.length < 1) {
      response.status(400).send(this.lib.APIMessage.ERROR.SUB_FORUM_IS_NOT_GLOBAL_THEME());
      return;
    }
    let timer = this.lib.performance();

    // Integer value from id
    parent.clearId = parent.id.replace( /[^\d.]/g, '' );

    /* Validate theme id by len:
     * 1) Len of them id less then minimal len of id (2)
     * 2) Len of theme id less then max sub forum count (2). Example: ОПЗ1 (4 len max)
    */
    if (parent.id.length < 2) {
      response.status(400).send(this.lib.APIMessage.ERROR.SUB_FORUM_IS_NOT_GLOBAL_THEME());
      return;
    } else if (parent.clearId.length >= 4) {
      response.status(400).send(this.lib.APIMessage.ERROR.MAX_SUB_FORUM_THEMES());
      return;
    }

    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      generateId(`${parent.id.substr(0, parent.id.length - parent.clearId.length)}${themeInfo.name.charAt(0)}`).then(generatedId => {
        this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else if (item.length === 0) response.status(204).send();
          else {
            let currentStructure = item[parent.index[0]];
            if (parent.index.length === 1) {
              currentStructure.subroot.push({
                id: generatedId,
                root: themeInfo.name,
                subroot: []
              });
            } else if (parent.index.length === 2) {
              if (!currentStructure.subroot[parent.index[1]].subroot) currentStructure.subroot[parent.index[1]].subroot = [];
              currentStructure.subroot[parent.index[1]].subroot.push({
                id: generatedId,
                root: themeInfo.name,
              })
            }

            this.lib.db.collection('forum-structure').updateOne({_id: this.lib.ObjectID(currentStructure._id)}, {$set: currentStructure}, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else {
                this.lib.log.trace(`Method addSubthemeToForum - метод проработал ${(timer - this.lib.performance()).toFixed(3)} мс`);
                response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
              }
            });
          }
        });
      }).catch(err => {
        response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для удаления подразделов
   * */
  removeSubThemeFromForum(request, response) {
    this.lib.log.trace('Method removeSubThemeFromForum - вызван');
    if (!request.params.token || !request.body.parentInfo) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method removeSubThemeFromForum - прошел валидацию параметров');
    const parent = this.helper.tryParseJson(request.body.parentInfo);
    if (!parent || !parent.id || !parent.index || !Array.isArray(parent.index) || parent.index.length < 1) {
      response.status(400).send(this.lib.APIMessage.ERROR.SUB_FORUM_IS_NOT_GLOBAL_THEME());
      return;
    }
    parent.children = parent.children || [];

    this.helper.searchUserByTokenAndValidateAdminRights(request.params.token).then(user => {
      this.lib.db.collection('forum-structure').find({}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(204).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
        else {
          const deleteTopicsPromise = (themeId) => {
            return new Promise((resolve, reject) => {
              if (!themeId) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR('ThemeIdIsNull')});

              this.lib.db.collection('forum').deleteMany({themeId: themeId }, (err, item) => {
                if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
                else resolve();
              });
            })
          };

          let currentStructure = item[parent.index[0]], waiting = [];
          if (parent.index.length === 1) {
            this.getIds().then(idsData => {
              for (let i = 0; i < idsData.ids[parent.index[0]].length; i++) {
                waiting.push(deleteTopicsPromise(idsData.ids[parent.index[0]][i]));
              }
            }).catch(err => {
              response.status(err.status).send(err.message);
            })
          } else if (parent.index.length === 2) {
            for (let i = 0; i < parent.children.length; i++)
              waiting.push(deleteTopicsPromise(parent.children[i].id));

            waiting.push(deleteTopicsPromise(parent.id));
          } else if (parent.index.length === 3)
            waiting.push(deleteTopicsPromise(parent.id));

          // Waiting until delete a forum topic's complete
          Promise.all(waiting).then(data => {
            // Next step after deleting is complete: update the structure

            // If we have a root theme
            // Than just delete root with sub themes
            if (parent.index.length === 1) {
              if (!currentStructure || !currentStructure._id) {
                this.lib.log.error('Method removeSubThemeFromForum - error. Debug info!');
                this.lib.log.error(`Method removeSubThemeFromForum - Data: ${currentStructure}`);
                response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR('Trouble...'));
                return;
              }

              this.lib.db.collection('forum-structure').deleteOne({_id: this.lib.ObjectID(currentStructure._id)}, (err, item) => {
                if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
                else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
              });
            } else if (parent.index.length >= 2) {
              if (parent.index.length === 2) {
                currentStructure.subroot.splice(parent.index[1], 1);
              } else if (parent.index.length === 3) {
                currentStructure.subroot[parent.index[1]].subroot.splice(parent.index[2], 1);
              }

              this.lib.db.collection('forum-structure').updateOne({_id: this.lib.ObjectID(currentStructure._id)}, {$set: currentStructure}, (err, item) => {
                if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
                else response.status(200).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
              });
            }
          }).catch(err => {
            response.status(err.status).send(err.message);
          })
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = Forum;
