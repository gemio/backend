module.exports = (app, lib) => {
  const News = require('./news');
  const route = new News(lib);

  app.use('/news', (req, res, next) => {
    lib.log.info(`URL: /news${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Add new news
  app.post('/news/:token', (req, res) => route.addNews(req, res));
  // Get preview news by count
  app.get('/news/preview', (req, res) => route.getPreviewNews(req, res));
  // Get full news by id
  app.get('/news', (req, res) => route.getNewsById(req, res));
  // Search a news by title
  app.get('/news/search', (req, res) => route.searchNews(req, res));

  // add comment to news
  app.post('/news/comment/:token', (req, res) => route.addCommentToNews(req, res));
};
