module.exports = (app, lib) => {
  require('./security')(app, lib);
  require('./admin')(app, lib);
  require('./users')(app, lib);
  require('./reminders')(app, lib);
  require('./messages')(app, lib);
  require('./friends')(app, lib);
  require('./role')(app, lib);
  require('./notification')(app, lib);
  require('./news')(app, lib);
  require('./moneyHelp')(app, lib);
  require('./posts')(app, lib);
  require('./search')(app, lib);
  require('./jobs')(app, lib);
  require('./specialist')(app, lib);
  require('./forum')(app, lib);
  require('./library')(app, lib);
  require('./doctor')(app, lib);
  require('./cron')(app, lib);
};
