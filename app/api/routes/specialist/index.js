module.exports = (app, lib) => {
  const Specialist = require('./specialist');
  const route = new Specialist(lib);

  app.use('/specialist', (req, res, next) => {
    lib.log.info(`URL: /specialist${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' || req.method === 'PATCH' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Add new question
  app.post('/specialist/question/:token', (req, res) => route.addQuestion(req, res));

  // Answer the question
  app.patch('/specialist/question/:token', (req, res) => route.answerTheQuestionById(req, res));

  // Get question by id
  app.get('/specialist/question/:questionId', (req, res) => route.getQuestionById(req, res));

  // Get all questions by theme id
  app.get('/specialist/questions/theme', (req, res) => route.getQuestionsByThemeId(req, res));

  // Get all specialists
  app.get('/specialists', (req, res) => route.getAllSpecialist(req, res));

  // Get themes by specialist name
  app.get('/specialist/themes', (req, res) => route.getThemesBySpecialist(req, res));

  // Close question only by user
  app.patch('/specialist/question/:token/close', (req, res) => route.closeQuestionById(req, res));

  // Getting a write status on question
  app.get('/specialist/question/canWrite/:token', (req, res) => route.canUserAnswerTheQuestion(req, res));

  // Get full structure of specialist-questions
  app.get('/specialist/structure', (req, res) => route.getFullStructureOfQuestions(req, res));

  // Get user questions
  app.get('/specialist/questions/user/:token', (req, res) => route.getUserQuestions(req, res));
};
