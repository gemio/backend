class Specialist {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
    this.validateSpecialist = (specialist) => {
      return new Promise((resolve, reject) => {
        this.lib.db.collection('specialist-question-structure').findOne({root: specialist}, (err, item) => {
          if (err) reject({status: 500, message: this.lib.APIMessage.ERROR.SERVER_ERROR(err)});
          else if (item === null) reject({status: 301, message: 'Специалист не найден!'});
          else resolve(item);
        });
      });
    };

    this.validateTheme = (specialist, userTheme) => {
      return new Promise((resolve, reject) => {
        this.validateSpecialist(specialist).then(data => {
          let tmp = data.themes.filter(theme => theme.text === userTheme);
          if (tmp.length > 0) resolve(Object.assign({}, tmp[0], {specialist: data.root}));
          else reject({status: 301, message: this.lib.APIMessage.ERROR.QUESTION_THEME_NOT_FOUND()});
        }).catch(err => {reject(err)});
      });
    }
  }

  /**
   * Метод для добавления нового вопроса
   * */
  addQuestion(request, response) {
    this.lib.log.trace('Method "addQuestion" - вызван');
    if (!request.params.token || !request.body.specialist || !request.body.theme || !request.body.text) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "addQuestion" - прошел валидацию параметров');
    this.validateTheme(request.body.specialist, request.body.theme).then((theme) => {
      this.helper.searchUserByToken(request.params.token).then(user => {
        let question = {
          owner: {
            fullName: this.helper.getFullName(user.public.fullName),
            id: user.id
          },
          themeId: theme.id,
          specialist: theme.specialist,
          text: request.body.text,
          comments: [],
          dateCreated: new Date().toISOString()
        };
        this.lib.db.collection('specialist-question').insertOne(question, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({id: item.ops[0]._id}));
        });
      }).catch(err => {
        response.status(err.status || 500).send(err.message);
      })
    }).catch(err => {
      response.status(err.status || 500).send(err.message);
    })
  }

  /**
   * Метод для получения информации о вопросе через его id
   * */
  getQuestionById(request, response) {
    this.lib.log.trace('Method "getQuestionById" - вызван');
    if (!request.params.questionId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getQuestionById" - прошел валидацию параметров');
    this.lib.db.collection('specialist-question').findOne({_id: this.lib.ObjectID(request.params.questionId)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(204);
      else {
        for (let i = 0; i < item.comments.length; i++)
          item.comments[i].user.avatar = this.helper.getUserAvatarByID(item.comments[i].user.userID);
        item.owner.avatar = this.helper.getUserAvatarByID(item.owner.id);
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
      }
    });
  }

  /**
   * Метод для получения информации о вопросах через id темы
   * */
  getQuestionsByThemeId(request, response) {
    this.lib.log.trace('Method "getQuestionsByTheme" - вызван');
    if (!request.query.themeId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getQuestionsByTheme" - прошел валидацию параметров');
    this.lib.db.collection('specialist-question').find({themeId: request.query.themeId}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204).send();
      else {
        for (let question of item) {
          question.comments = question.comments.length;
          question.owner.avatar = this.helper.getUserAvatarByID(question.owner.id);
        }

        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
      }
    });
  }

  /**
   * Метод для получения информации обо всех специалистах
   * */
  getAllSpecialist(request, response) {
    this.lib.log.trace('Method "getAllSpecialist" - вызван');
    this.lib.db.collection('specialist-question-structure').find({}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204);
      else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({specialists: item.map(structure => structure.root)}));
    });
  }

  /**
   * Метод для получения информации о темах через название специалиста
   * */
  getThemesBySpecialist(request, response) {
    this.lib.log.trace('Method "getThemesBySpecialist" - вызван');
    if (!request.query.specialist) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getThemesBySpecialist" - прошел валидацию параметров');
    this.validateSpecialist(request.query.specialist).then(root => {
      response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({themes: root.themes}));
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  /**
   * Метод для получения полной структуры специалист->темы
   * */
  getFullStructureOfQuestions(request, response) {
    this.lib.log.trace('Method "getFullStructureOfQuestions" - вызван');
    this.lib.db.collection('specialist-question-structure').find({}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item.length === 0) response.status(204).send();
      else {
        let structure = item.map(specialist => {
          return {
            specialist: specialist.root,
            themes: specialist.themes
          }
        });
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(structure));
      }
    });
  }

  /**
   * Метод для закрытия вопроса
   * */
  closeQuestionById(request, response) {
    this.lib.log.trace('Method "closeQuestionById" - вызван');
    if (!request.params.token || !request.query.questionId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "closeQuestionById" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('specialist-question').findOne({_id: this.lib.ObjectID(request.query.questionId)}, (err, question) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (question === null) response.status(204).send();
        else {
          if (question.owner.id !== user.id) response.status(403).send(this.lib.APIMessage.ERROR.QUESTION_NOT_OWNER());
          this.lib.db.collection('specialist-question').updateOne({_id: this.lib.ObjectID(request.query.questionId)}, {$set: {isClosed: true}}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
            else response.send(this.lib.APIMessage.SUCCESS.QUESTION_CLOSED_SUCCESS());
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для проверки возможности пользователя писать сообщение в вопросе
   * */
  canUserAnswerTheQuestion(request, response) {
    this.lib.log.trace('Method "canUserAnswerTheQuestion" - вызван');
    if (!request.params.token || !request.query.questionId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "canUserAnswerTheQuestion" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('specialist-question').findOne({_id: this.lib.ObjectID(request.query.questionId)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item === null) response.status(204).send();
        else {
          if (user.role.name === 'Специалист-консультант' && user.role.specialist !== item.specialist)
            response.status(406).send(this.lib.APIMessage.ERROR.ANSWER_WRONG_SPECIALIST(item.specialist));
          else if ((user.role.name === 'Специалист-консультант' && user.role.specialist === item.specialist) || user.id === item.owner.id) {
            if (item.isClosed) response.status(406).send(this.lib.APIMessage.ERROR.QUESTION_ALREADY_CLOSED());
            else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE({canWriteMessage: true}));
          } else response.status(406).send(this.lib.APIMessage.ERROR.ANSWER_WRONG_USER());
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  getUserQuestions(request, response) {
    this.lib.log.trace('Method "getUserQuestions" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getUserQuestions" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('specialist-question').find({"owner.id": user.id}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(204).send();
        else {
          let questions = item.filter(question => !question.isClosed);
          if (questions.length === 0) response.status(204).send();
          else {
            for (let i = 0; i < questions.length; i++) {
              questions[i].comments = questions[i].comments.length;
              questions[i].owner.avatar = this.helper.getUserAvatarByID(questions[i].owner.id);
            }
            response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(questions));
          }
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  /**
   * Метод для отправки ответа на вопрос
   * */
  answerTheQuestionById(request, response) {
    this.lib.log.trace('Method "answerTheQuestionById" - вызван');
    if (!request.params.token || !request.body.message || !request.query.questionId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "answerTheQuestionById" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('specialist-question').findOne({_id: this.lib.ObjectID(request.query.questionId)}, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item === null) response.status(204).send();
        else {
          if (user.role.name === 'Специалист-консультант' && user.role.specialist !== item.specialist)
            response.status(406).send(this.lib.APIMessage.ERROR.ANSWER_WRONG_SPECIALIST(item.specialist));
          else if ((user.role.name === 'Специалист-консультант' && user.role.specialist === item.specialist) || user.id === item.owner.id) {
            let comments = item.comments;
            comments.push({
              user: {
                userID: user.id,
                fullName: this.helper.getFullName(user.public.fullName)
              },
              text: request.body.message,
              date: new Date().toISOString()
            });

            this.lib.db.collection('specialist-question').updateOne({_id: this.lib.ObjectID(request.query.questionId)}, {$set: {comments: comments}}, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
            });
          } else response.status(406).send(this.lib.APIMessage.ERROR.ANSWER_WRONG_USER());
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}


module.exports = Specialist;
