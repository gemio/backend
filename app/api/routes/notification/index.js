module.exports = (app, lib) => {
  const Notification = require('./notification');
  const route = new Notification(lib);

  app.use('/notification', (req, res, next) => {
    lib.log.info(`URL: /notification${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Routing
  // Get count notification
  app.get('/notification/:token', (req, res) => route.getCountNotification(req, res));
};
