module.exports = (app, lib) => {
  const Cron = require('./cron');
  const cron = require('node-cron');
  const logger = require('log4js');
  const log = logger.getLogger('CRON TASK');
  logger.configure(lib.config.log);
  const route = new Cron({lib: lib, cronLogger: log});

  cron.schedule('0 0 0 * * *', () => {
    log.trace('Cron is started');
    route.alertUserOnRemindBeforeDay();
  });
};
