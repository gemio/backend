module.exports = (app, lib) => {
  const Reminder = require('./reminders');
  const route = new Reminder(lib);

  app.use('/remind', (req, res, next) => {
    lib.log.info(`URL: /remind${req.url}, Method: ${req.method}`);
    lib.log.debug(req.method === 'POST' ? `Body: ${JSON.stringify(req.body, null, '\t')}`  : `Query: ${JSON.stringify(req.query, null, '\t')}`);
    next();
  });

  // Routing
  // Get remind type
  app.get('/remind/types', (req, res) => route.getRemindsType(req, res));
  // Add new remind
  app.post('/remind', (req, res) => route.addRemind(req, res));
  // Get all reminds or search via id
  app.get('/remind/:token', (req, res) => route.getReminds(req, res));
  // Remove remind by id
  app.delete('/remind/:token', (req, res) => route.removeRemindByID(req, res));
  // Remove all reminds
  app.delete('/remind/:token')
};
