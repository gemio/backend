class Reminder {
  constructor(lib) {
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для получения всех типов напоминаний
   * */
  getRemindsType(request, response) {
    this.lib.db.collection('remindType').find({}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(item));
    });
  }

  /**
   * Метод для добавления напоминания
   * */
  addRemind(request, response) {
    if (!request.body.date || !request.body.type || !request.body.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    let remind = this.getRemindObject(request.body, request.body.type);
    if (remind === undefined) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    } else if (typeof remind === 'object' && remind.message && remind.message.type === 'error') {
      response.status(406).send(remind);
      return;
    }

    this.helper.searchUserByToken(request.body.token).then(user => {
      let countReminds = user.reminds.length;
      remind = {
        $push: {reminds: remind}
      };
      Object.assign(remind.$push.reminds, {id: countReminds});
      this.lib.db.collection('users').updateOne({_id: new this.lib.ObjectID(request.body.token)}, remind, (err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
        else response.send(this.lib.APIMessage.SUCCESS.REMIND_CREATED(remind.$push.reminds));
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для удаления напоминаний пользователя: все или один (через id)
   * */
  removeRemindByID(request, response) {
    this.lib.log.trace('Метод "removeRemindByID" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Метод "removeRemindByID" - прошел валидацию');
    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.log.trace('Метод "removeRemindByID" - id напоминания найден');
      let reminds = user.reminds;
      if (reminds.length === 0) response.status(204).send(this.lib.APIMessage.ERROR.NO_REMIND());
      else {
        let deleteRemind = {$pull: { "reminds": {"id": request.query.id ? +request.query.id : {$gte: 0}}}};
        this.lib.db.collection('users').updateOne({_id: new this.lib.ObjectID(request.params.token)}, deleteRemind, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
          else {
            this.lib.log.trace('Метод "removeRemindByID" - напоминание успешно удалено');
            response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
          }
        });
      }
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения напоминаний по: id, диапазону, определнный тип (план или напоминание)
   * */
  getReminds(request, response) {
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    let IDSearch = request.query.id;
    let rangeSearch = request.query.range;
    let planSearch = request.query.isPlan;
    this.helper.searchUserByToken(request.params.token).then(user => {
      if (IDSearch) {
        if (IDSearch >= user.reminds.length || IDSearch < 0) response.status(400).send(this.lib.APIMessage.ERROR.WRONG_REMIND_ID());
        else response.send(user.reminds[IDSearch]);
      } else if (rangeSearch && !planSearch) {
        let range = rangeSearch.split('IO');
        if (range.length === 0 && range.length > 2) {
          response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
          return;
        }

        let dateFrom = range[0].split('.');
        let dateTo = range[1].split('.');
        let date = {
          from: new Date(+dateFrom[2], +dateFrom[1], +dateFrom[0]).setHours(0, 0, 0, 0),
          to: new Date(+dateTo[2], +dateTo[1], +dateTo[0]).setHours(0, 0, 0, 0)
        };
        if (date.from == 'Invalid Date' || date.to == 'Invalid Date' || date.from > date.to) {
          response.status(406).send(this.lib.APIMessage.ERROR.WRONG_DATE_FORMAT());
          return;
        }

        let reminds = user.reminds;
        let remindsByRange = reminds.filter((remind) => {
          if (remind.isPlan === undefined || remind.isPlan || !remind.date) return false;
          let tempDate = remind.date.split('.');
          let remindDate = new Date(+tempDate[2], +tempDate[1], +tempDate[0]).setHours(0, 0, 0, 0);
          return remindDate >= date.from && remindDate <= date.to && !remind.isPlan;
        });

        if (remindsByRange.length === 0) response.status(412).send({message: {type: 'SUCCESS', message: 'Нет напоминаний на указанный диапазон'}});
        else response.send({reminds: remindsByRange, count: remindsByRange.length});
      } else if (planSearch) {
        let reminds = user.reminds;
        let currentDate = new Date();
        let remindsPlan = reminds.filter(remind => remind.isPlan && new Date(remind.startDate) >= currentDate);
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(remindsPlan));
      }
      else {
        let reminds = [], currentDate = new Date();
        for (let i = 0; i < user.reminds.length; i++) {
          if (user.reminds[i].isPlan) {
            let date = new Date(user.reminds[i].startDate);
            user.reminds[i].date = `${date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()}.${date.getMonth() < 10 ? `0${date.getMonth()}` : date.getMonth()}.${date.getFullYear()}`;
          }

          let dateRaw = user.reminds[i].date.split('.');
          let date = new Date(+dateRaw[2], +dateRaw[1], +dateRaw[0]);
          if (date != 'Invalid Date') {
            if (date >= currentDate) {
              reminds.push(user.reminds[i]);
            }
          }
        }

        response.send({reminds: reminds, count: reminds.length});
      }
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения объекта с информацией о напоминании в зависимости от типа
   * @param {string} param GET запрос с параметрами
   * @param {string} type Тип напоминания
   * @return {object} Объект с информацие о напоминании
   * */
  getRemindObject(param, type) {
    this.lib.log.trace('Валидация полученных данных о напоминании...');
    this.lib.log.trace(`Получен тип: ${type}`);
    switch (type.toLowerCase()) {
      case 'профилактика': {
        let check = param.dosage && param.medicament && param.time && this.validateTimeOfRemind(param.time) && param.date && this.validateDateOfRemind(param.date);
        this.lib.log.trace(`Время: ${this.validateTimeOfRemind(param.time) ? 'успешно!' : 'провал'}`);
        this.lib.log.trace(`Дата: ${this.validateDateOfRemind(param.date) ? 'успешно!' : 'провал'}`);
        if (!check) return undefined;
        this.lib.log.trace('Валидация прошла успешно!');
        return {
          type: type,
          text: `Необходимо произвести инъекцию ${param.medicament} ${param.dosage}ME`,
          date: param.date,
          time: param.time,
          isPlan: false
        };
      }
      case 'напоминание': {
        let paramCheck = param.text && param.time && param.date;
        let dateValidCheck = this.validateDateOfRemind(param.date);
        let dateBadCheck = this.checkForBadDate(param.date);
        let timeCheck = this.validateTimeOfRemind(param.time);
        this.lib.log.trace(`Дата: ${dateValidCheck && dateBadCheck ? 'успешно!' : 'провал'}`);
        this.lib.log.trace(`Время: ${timeCheck ? 'успешно!' : 'провал'}`);
        if (!paramCheck) return undefined;
        else if (!dateValidCheck) return this.lib.APIMessage.ERROR.WRONG_DATE_FORMAT();
        else if (!dateBadCheck) return this.lib.APIMessage.ERROR.DATE_CAN_NOT_BE_LESS_THAN_CURRENT();
        else if (!timeCheck) return this.lib.APIMessage.ERROR.WRONG_TIME_FORMAT();
        this.lib.log.trace('Валидация прошла успешно!');
        return {
          type: type,
          text: param.text,
          date: param.date,
          time: param.time,
          isPlan: false
        }
      }
      default:
        return undefined;
    }
  }

  /**
   * Метод для валидации времени
   * @param {string} time Время в формате HH:mm
   * */
  validateTimeOfRemind(time) {
    let splited = time.split(':');
    let hourCheck = splited[0] >= 0 && splited[0] < 24;
    let minuteCheck = splited[1] >= 0 && splited[1] < 60;
    let lengthCheck = splited.length === 2;
    return hourCheck && minuteCheck && lengthCheck;
  }

  /**
   * Метод для валидации даты
   * @param {string} date Дата в формате dd.mm.yyyy
   * */
  validateDateOfRemind(date) {
    let currentDate = new Date();
    let remindDate = date.split('.');
    date = new Date(+remindDate[2], +remindDate[1], +remindDate[0]);
    return date != 'Invalid Date';
  }

  checkForBadDate(date) {
    let currentDate = new Date();
    let remindDate = date.split('.');
    date = new Date(+remindDate[2], +remindDate[1], +remindDate[0]);
    return currentDate - date < 86400000;
  }
}

module.exports = Reminder;
