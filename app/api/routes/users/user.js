class User {
  constructor(lib) {
    this.md5 = require('crypto-md5');
    this.lib = lib;
    this.helper = lib.helper;
  }

  /**
   * Метод для авторизации
   * */
  auth(request, response) {
    this.lib.log.trace('Method "auth" - вызван');
    const param = {
      'login': request.query.login || undefined,
      'password': request.query.password ? this.md5(request.query.password, 'hex') : undefined
    };

    if (!param.login || !param.password || request.query.password === '' || request.query.login === '') {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.log.trace('Method "auth" - параметры прошли валидацию');
    this.lib.db.collection('users').findOne(param, (err, item) => {
      this.lib.log.trace('Method "auth" - получены данные из бд');
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null) response.status(401).send(this.lib.APIMessage.ERROR.WRONG_CREDITIONAL());
      else {
        let user = item;
        if (user.statusRegister === this.helper.ENUM.statusRegister.NOT_CONFIRM.EMAIL)
          response.status(423).send(this.lib.APIMessage.ERROR.AUTH_NOT_CONFIRMED_EMAIL());
        else if (user.statusRegister === this.helper.ENUM.statusRegister.NOT_CONFIRM.ADMIN)
          response.status(423).send(this.lib.APIMessage.ERROR.AUTH_NOT_CONFIRMED_ADMIN({_id: user._id}));
        else
          response.send(this.lib.APIMessage.CREATOR.createSuccessMessage('', this.helper.getAuthUserInfo(user)));
      }
    });
  }

  /**
   * Авторизация через токен
   * */
  authWithToken(request, response) {
    this.lib.log.trace('Method "authWithToken" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "authWithToken" - параметры прошли валидацию');
    this.helper.searchUserByToken(request.params.token).then(user => {
      response.send(this.lib.APIMessage.CREATOR.createSuccessMessage('', this.helper.getAuthUserInfo(user)));
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для регистрации
   * */
  register(request, response) {
    this.lib.log.trace('Method "register" - вызван');
    request.body = JSON.parse(JSON.stringify(request.body));
    if (!request.body.password || !request.body.email || !request.body.firstname || !request.body.surname || !request.body.role) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.log.trace('Method "register" - параметры прошли валидацию');
    const user = {
      login: request.body.email,
      password: this.lib.md5(request.body.password, 'hex'),
      email: request.body.email,
      role: {name: request.body.role},
      reminds: []
    };

    // Validate user role
    // If role isn't right or our role is restrict (e.g. admin or moderator)
    if (this.helper.checkForRoleName(user.role.name) || this.helper.checkForRoleAccess(user.role.name)) {
      response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      return;
    }

    // Check for exist user
    this.helper.isUserExists({login: user.login, email: user.email})
      .then(() => {
        response.status(409).send(this.lib.APIMessage.ERROR.USER_EXIST());
      }).catch(() => {
        // If user is not exists
        // Than register
        user.id = this.lib.shortid.generate();
        user.public = {
          fullName: {
            name: request.body.firstname,
            surname: request.body.surname,
            lastname: ''
          },
          social: {
            facebook: '',
            vk: '',
            googlePlus: ''
          },
          birthday: '',
          city: '',
          hemoStatus: '',
          about: '',
          status: ''
        };
        user.phone = '';
        user.statusRegister = this.helper.ENUM.statusRegister.NOT_CONFIRM.EMAIL;
        user.role.id = this.helper.getRoleID(user.role.name);
        if (user.role.name === 'Специалист-консультант') user.role.specialist = 'Врач';
        user.therapist = [];
        user.privacy = {
          allowReadForm: 'true',
          allowReadContacts: 'true',
          allowFriendRequest: 'true',
          allowComments: 'true',
          allowMessages: 'true'
        };
        user.relative = [];
        user.medicalDocuments = [];
        user.registerDate = new Date();

        // validating user data
        // full name, email and password
        if (!/^[а-яА-Я]+$/.test(user.public.fullName.name) || !/[а-яА-ЯёЁ]/.test(user.public.fullName.surname) 
            || !/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/.test(user.email) || !/[a-zA-Z0-9]{8}/.test(user.password)) {
          response.status(406).send(this.lib.APIMessage.ERROR.PARAMS_VALIDATE_ERROR());
          return;
        }

        // Insert user into table
        this.lib.db.collection('users').insertOne(user, (err, item) => {
          this.lib.log.trace('Method "register" - данные были отправлены в бд');
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
          else {
            this.lib.log.trace(`Method "register" - отправка письма на почту ${user.email}`);
            let link = `http://${this.helper.apiHost}/user/confirmRegister/${item.ops[0]._id}`;
            this.lib.email.send(this.lib.emailTemplate.registerTemplate(user.email, link), (err, mess) => {
              if (err) {
                response.status(512).send(this.lib.APIMessage.ERROR.WRONG_EMAIL());
                this.lib.db.collection('users').deleteOne({_id: new this.lib.ObjectID(item.ops[0].idUser)});
                this.lib.log.error(err);
              }
              else {
                response.status(200).send(this.lib.APIMessage.SUCCESS.REGISTRATION_SUCCESS(user.email));

                // Copy a default avatar to static image folder
                this.lib.fs.createReadStream(`app/static/avatar/default.jpg`).pipe(this.lib.fs.createWriteStream(`app/static/avatar/${user.id}.jpg`));
                this.lib.log.trace(`Method "register" - регистрация пользователя завершена`);
              }
            });
          }
        });
    });
  }

  /**
   * Метод для подтверждения регистрации
   * */
  confirmRegistration(request, response) {
    this.lib.log.trace('Method "confirmRegistration" - вызван');
    if (!request.params.id) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "confirmRegistration" - прошел валидацию параметров');
    const front = this.lib.config.setting.frontend;
    const redirectURL = `http://${front.host}${front.subFolder}login`;
    let script = '<script>setTimeout(() => {window.location ="' + redirectURL + '"}, 3000)</script>';
    this.lib.db.collection('users').findOne({_id: this.lib.ObjectID(request.params.id)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null)
        response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        script += item.statusRegister !== this.helper.ENUM.statusRegister.NOT_CONFIRM.EMAIL ?
          'Аккаунт уже подтвержден!' :
          'Аккаунт подтвержден. Перенаправление на страницу авторизации произойдет через 3 секунды...';
        if (item.statusRegister === this.helper.ENUM.statusRegister.NOT_CONFIRM.EMAIL) {
          this.lib.db.collection('users').updateOne({_id: this.lib.ObjectID(request.params.id)}, {$set: {statusRegister: this.helper.ENUM.statusRegister.NOT_CONFIRM.ADMIN}}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
            else response.send(script);
          });
        } else response.send(script);
      }
    });
  }

  /**
   * Метод для обновления пароля
   * */
  updatePassword(request, response) {
    this.lib.log.trace('Method "resetPassword" - вызван');
    const param = {
      'login': request.body.login || undefined,
      'password': request.body.password ? this.lib.md5(request.body.password, 'hex') : undefined,
      'newPassword': request.body.newPassword ? this.lib.md5(request.body.newPassword, 'hex') : undefined,
    };

    this.helper.isUserExists({login: param.login, email: param.login})
      .then((item) => {
        // If we found user
        // Than update password
        let newItem = {$set: { password: param.newPassword }};
        this.lib.db.collection('users').updateOne(item, newItem, (err, item) => {
          this.lib.log.trace('Method "resetPassword" - данные были отправлены в бд');
          if (err)
            response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else
            response.send(this.lib.APIMessage.CREATOR.createSuccessMessage('', {'oldPassword': request.body.password, 'newPassword': request.body.newPassword}));
        });
      }).catch((item) => {
        response.status(409).send(this.lib.APIMessage.ERROR.WRONG_CREDITIONAL());
    });
  }

  /**
   * Метод для получения ссылки для сброса пароля
   * */
  getResetPasswordLink(request, response) {
    this.lib.log.trace('Метод "getResetPasswordLink" вызван');
    if (!request.query.email) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Метод "getResetPasswordLink" - прошел валидацию параметров');
    this.lib.db.collection('users').findOne({email: request.query.email}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null) response.status(401).send(this.lib.APIMessage.CREATOR.createErrorMessage('Введенный E-mail не зарегистрирован в системе!'));
      else {
        let front = this.lib.config.setting.frontend;
        let resetLink = `http://${front.host}${front.subFolder}resetPassword/${item._id}`;
        this.lib.email.send(this.lib.emailTemplate.resetPasswordTemplate(request.query.email, resetLink), (err, mess) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(''));
          else {
            this.lib.log.trace('Метод "getResetPasswordLink" - отправка письма завершена!');
            response.status(200).send(this.lib.APIMessage.SUCCESS.RESET_PASSWORD_LINK_SUCCESS());
          }
        });
      }
    });
  }

  /**
   * Метод для сброса пароля
   * */
  resetPassword(request, response) {
    if (!request.params.id || !request.query.password) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.db.collection('users').findOne({_id: new this.lib.ObjectID(request.params.id)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
      else if (item === null) response.status(401).send(this.lib.APIMessage.ERROR.WRONG_EMAIL());
      else {
        let newData = {$set: {password: this.lib.md5(request.query.password, 'hex')}};
        this.lib.db.collection('users').updateOne({_id: new this.lib.ObjectID(request.params.id)}, newData, (err, item) => {
          if (err) response.status(500).send(this.lib.APIMessage.CREATOR.createErrorMessage(`Произошла ошибка! ${err}`));
          else response.send(this.lib.APIMessage.SUCCESS.RESET_PASSWORD_SUCCESS())
        });
      }
    });
  }

  /**
   * Метод для обновления публичной информации пользователя
   * */
  updatePublicInformation(request, response) {
    this.lib.log.trace('Method "updatePublicInformation" - вызван');
    if (!request.body || !request.body.fullName || !request.body.social) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "updatePublicInformation" - прошел валидацию параметров');
    request.body.fullName = JSON.parse(request.body.fullName);
    request.body.social = JSON.parse(request.body.social);
    const userInfo = {
      credits: {
        login: request.body.login,
        token: request.body.token
      },
      fullName: {
        name: request.body.fullName.name || '',
        surname: request.body.fullName.surname || '',
        lastname: request.body.fullName.lastname || '',
      },
      birthday: request.body.birthday || '',
      city: request.body.city || '',
      hemoStatus: request.body.hemoStatus || '',
      about: request.body.about || '',
      status: request.body.status || '',
      social: {
        facebook: request.body.social.facebook || '',
        vk: request.body.social.vk || '',
        googlePlus: request.body.social.google || ''
      },
      phone: request.body.phone || ''
    };

    if (request.body.roleSpecialist)
      userInfo.roleSpecialist = request.body.roleSpecialist;

    let now = new Date();
    let birtdate = new Date(userInfo.birthday);
    if (birtdate == 'Invalid Date') {
      response.status().send(this.lib.APIMessage.ERROR.WRONG_DATE());
      return;
    } else if (birtdate > now) {
      response.status().send(this.lib.APIMessage.CREATOR.createErrorMessage('Дата рождения не может быть больше, чем сегодняшняя!'));
      return;
    }

    this.helper.searchUserByToken(userInfo.credits.token)
      .then(user => {
        let updateInfo = {
          $set: {
            "public": {
              "fullName": userInfo.fullName,
              "birthday": userInfo.birthday,
              "city": userInfo.city,
              "hemoStatus": userInfo.hemoStatus,
              "about": userInfo.about,
              "social": userInfo.social,
              "status": userInfo.status
            },
            "phone": userInfo.phone
          }
        };

        // Change user specialist name by validating a role (should be a specialist)
        if (userInfo.roleSpecialist && user.role.name === 'Специалист-консультант') {
          updateInfo.$set.role = {
            specialist: userInfo.roleSpecialist,
            name: user.role.name,
            id: user.role.id
          };
        }

        this.lib.db.collection('users').updateOne({'_id': new this.lib.ObjectID(userInfo.credits.token)}, updateInfo, (err, item) => {
          this.lib.log.trace('Method "updatePublicInformation" - данные были отправлены в бд');
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else if (item === null) response.status(520).send(this.lib.APIMessage.CREATOR.createErrorMessage('Неизвестная ошибка'));
          else {
            this.lib.log.debug(`Modified: ${JSON.parse(item).nModified}`);
            this.lib.db.collection('users').findOne({'_id': new this.lib.ObjectID(userInfo.credits.token)}, (err, item) => {
              if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
              else response.send(this.lib.APIMessage.CREATOR.createSuccessMessage('Данные успешно изменены', this.helper.getAuthUserInfo(item)));
            });
          }
        });
      })
      .catch(err => {
        response.status(err.status).send(err.message);
      });
  }

  /**
   * Метод для обновления настроек приватности
   * */
  updatePrivacyInformation(request, response) {
    this.lib.log.trace('Method "updatePrivacyInformation" - вызван');
    if (!request.body.allowReadForm ||
        !request.body.allowReadContacts ||
        !request.body.allowFriendRequest ||
        !request.body.allowComments ||
        !request.body.allowMessages ||
        !request.body.token || !request.body.login) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "updatePrivacyInformation" - прошел валидацию параметров');

    let privacy = {
      allowReadForm: request.body.allowReadForm,
      allowReadContacts: request.body.allowReadContacts,
      allowFriendRequest: request.body.allowFriendRequest,
      allowComments: request.body.allowComments,
      allowMessages: request.body.allowMessages,
    };
    this.helper.searchUserByToken(request.body.token).then(user => {
      let updateInfo = {
        $set: {
          "privacy": {
            "allowReadForm": privacy.allowReadForm,
            "allowReadContacts": privacy.allowReadContacts,
            "allowFriendRequest": privacy.allowFriendRequest,
            "allowComments": privacy.allowComments,
            "allowMessages": privacy.allowMessages,
          }
        }
      };
      this.lib.db.collection('users').updateOne({'_id': new this.lib.ObjectID(request.body.token)}, updateInfo, (err, item) => {
        if (err)response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item === null) response.status(520).send(this.lib.APIMessage.CREATOR.createErrorMessage('Неизвестная ошибка'));
        else {
          this.lib.db.collection('users').findOne({'_id': new this.lib.ObjectID(request.body.token)}, (err, item) => {
            if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
            else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(this.helper.getAuthUserInfo(item)));
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для обновления статуса пользователя
   * */
  updateStatus(request, response) {
    this.lib.log.trace('Method "updateStatus" - вызван');
    if (!request.params.token || !request.body.newStatus) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    if (request.body.newStatus === '!@#CLEAR!@#')
      request.body.newStatus = '';
    this.lib.log.trace('Method "updateStatus" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      let updateInfo = {$set: {"public.status": request.body.newStatus}};
      this.lib.db.collection('users').updateOne({'_id': this.lib.ObjectID(request.params.token)}, updateInfo, (err, item) => {
        this.lib.log.trace('Method "updateStatus" - данные были отправлены в бд');
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item === null) response.status(520).send(this.lib.APIMessage.CREATOR.createErrorMessage('Неизвестная ошибка'));
        else {
          user.public.status = request.body.newStatus;
          response.send(this.lib.APIMessage.CREATOR.createSuccessMessage('Данные успешно изменены', this.helper.getAuthUserInfo(user)));
        }
      });
    }).catch(err => {
      response.send(err.status).send(err.message);
    });
  }

  /**
   * Мето для получения настроек приватности
   * */
  getPrivacyInformation(request, response) {
    this.lib.log.trace('Method "getPrivacyInformation" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getPrivacyInformation" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(user.privacy));
    }).catch(err => {
      response.send(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения публичной пользовательской информации
   * */
  getPublicInformation(request, response) {
    this.lib.log.trace('Method "getPublicInformation" - вызван');
    if (!request.query.token && !request.query.userID) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getPublicInformation" - параметры прошли валидацию');

    let token = request.query.token, userID = request.query.userID;
    const search = userID ? {id: userID} : {'_id': new this.lib.ObjectID(token)};
    this.lib.db.collection('users').findOne(search, (err, item) => {
      this.lib.log.trace('Method "getPublicInformation" - данные из бд пришли и были отправлены клиенту');
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        let publicInfo = item.public;
        this.lib.db.collection('friends').find({$and: [{user1: this.lib.ObjectID(item._id)}, {status: 'accepted'}]}).count((err, count) => {
          if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
          else {
            publicInfo.role = item.role.name;
            publicInfo.countFriends = count;
            publicInfo.avatar = this.helper.getUserAvatarByID(item.id);
            publicInfo.id = item.id;
            publicInfo.hemoStatus = item.public.hemoStatus;
            response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(publicInfo));
          }
        });
      }
    });
  }

  /**
   * Метод для загрузки аватарки пользователя
   * */
  uploadAvatar(request, response, next) {
    this.lib.log.trace('Method "uploadAvatar" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return
    }
    this.lib.log.trace('Method "uploadAvatar" - прошел валидацию параметров');
    this.helper.searchUserByToken(request.params.token).then(user => {
      const storage = this.lib.multer.diskStorage({
        destination: (req, file, cb) => {
          cb(null, './app/static/avatar/')
        },
        filename: (req, file, cb) => {
          if (file.mimetype.includes('image') && req.params.token) {
            cb(null, `${user.id}.jpg`);
            response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
          }
          else cb(this.lib.APIMessage.ERROR.WRONG_FILE_EXT('jpg, jpeg, png'), false);
        }
      });
      const upload = this.lib.multer({ storage: storage }).single('avatar');
      upload(request, response, (err) => {
        if (err) response.status(400).send(err);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения публичной информации всех пользователей
   * */
  getUsersPublicInformation(request, response) {
    //TODO: Убрать токен из обязательных параметров
    this.lib.log.trace('Method "getUsersPublicInformation" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getUsersPublicInformation" - параметры прошли валидацию');
    this.lib.db.collection('users').find({statusRegister: this.helper.ENUM.statusRegister.CONFIRM}).toArray((err, item) => {
      if (err)
        response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else {
        let users = item.map(user => this.helper.getBasicUserInfo(user));
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
      }
    });
  }

  /**
   * Метод для получения публичной информации всех пользователей, которые разрешили сообщения
   * */
  getUsersThatAllowsMessage(request, response) {
    this.lib.log.trace('Method "getUsersPublicInformation" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.helper.searchUserByToken(request.params.token).then(user => {
      this.lib.db.collection('users').find({statusRegister: this.helper.ENUM.statusRegister.CONFIRM}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else if (item.length === 0) response.status(412).send(this.lib.APIMessage.CREATOR.createSuccessMessage('Нет пользователей'));
        else {
          let users = user.map(user => this.helper.getBasicUserInfo(user));
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для получения публичной информации всех пользователей, которые разрешили запросы в друзья
   * */
  getUsersThatAllowsFriend(request, response) {
    this.lib.log.trace('Method "getUsersPublicInformation" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getUsersPublicInformation" - параметры прошли валидацию');
    this.helper.searchUserByToken(request.params.token).then(userToken => {
      this.lib.db.collection('users').find({$and: [{"privacy.allowFriendRequest": "true" }, {statusRegister: this.helper.ENUM.statusRegister.CONFIRM}]}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else {
          let users = item.map(user => this.helper.getBasicUserInfo(user));
          users = users.filter(user => user.id !== userToken.id);
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Метод для поиска пользователей по имени
  */
  searchUserThatAllowsFriend(request, response) {
    this.lib.log.trace('Method "searchUserThatAllowsFriend" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "searchUserThatAllowsFriend" - параметры прошли валидацию');
    this.helper.searchUserByToken(request.params.token).then(userToken => {
      this.lib.db.collection('users').find({$and: [{"privacy.allowFriendRequest": "true"}, {statusRegister: this.helper.ENUM.statusRegister.CONFIRM}]}).toArray((err, item) => {
        if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
        else {
          let users = item.map(user => this.helper.getBasicUserInfo(user));
          users = users.filter(user => user.id !== userToken.id && user.fullName.toLowerCase().includes(decodeURIComponent(request.query.search.toLowerCase()) || ''));
          response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  searchUser(request, response) {
    this.lib.log.trace('Method "searchUser" - вызван');
    this.lib.db.collection('users').find({$and: [{"privacy.allowFriendRequest": "true"}, {statusRegister: this.helper.ENUM.statusRegister.CONFIRM}]}).toArray((err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else {
        let users = item.map(user => this.helper.getBasicUserInfo(user));
        users = users.filter(user => user.fullName.toLowerCase().includes(decodeURIComponent(request.query.search.toLowerCase() || '')));
        response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(users));
      }
    });
  }

  /**
   * Метод для загрузки изображения документа на верификацию
   * */
  addMedicalDocument(request, response, next) {
    this.lib.log.trace('Method "addMedicalDocument" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return
    }
    this.lib.log.trace('Method "addMedicalDocument" - прошел валидацию параметров');
    this.lib.db.collection('users').findOne({_id: this.lib.ObjectID(request.params.token)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        const storage = this.lib.multer.diskStorage({
          destination: function (req, file, cb) {
            cb(null, `./app/static/medicalDocuments/${request.params.token}/`)
          },
          filename: (req, file, cb) => {
            if (file.mimetype.includes('jpeg') || file.mimetype.includes('jpg') ||
                file.mimetype.includes('png') || file.mimetype.includes('gif') ||  file.mimetype.includes('pdf') && req.params.token) {
              let date = Date.now();
              let fileName = `${file.fieldname}-${date}${require('path').extname(file.originalname)}`;

              if (!this.lib.fs.existsSync(`./app/static/medicalDocuments/${request.params.token}`))
                this.lib.fs.mkdirSync(`./app/static/medicalDocuments/${request.params.token}`);

              cb(null, `${fileName}`);
              let document = {
                $push: {
                  medicalDocuments: {
                    img: fileName,
                    status: 'unverified',
                    id: this.lib.ObjectID().toString()
                  }
                }
              };
              this.lib.db.collection('users').updateOne({_id: this.lib.ObjectID(request.params.token)}, document, (err, item) => {
                if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
                else response.send(this.lib.APIMessage.CREATOR.createSuccessMessage('Документ успешно добавлен на модерацию.'));
              });
            } else cb(this.lib.APIMessage.ERROR.WRONG_FILE_EXT('jpg, jpeg, png, pdf'), false);
          }
        });
        const upload = this.lib.multer({storage: storage, limits: { fileSize: 3145728 }}).array('document');
        upload(request, response, (err) => {
          if (err) {
            if (err.code === 'LIMIT_FILE_SIZE') response.send(this.lib.APIMessage.ERROR.WRONG_FILE_SIZE());
            else response.status(400).send(err);
          }
        });
      }
    });
  }

  /**
   * Метод для получения информации о документах пользоваетеля
   * */
  getMedicalDocuments(request, response) {
    this.lib.log.trace('Method "getMedicalDocuments" - вызван');
    if (!request.params.token) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getMedicalDocuments" - вызван');
    this.lib.db.collection('users').findOne({_id: this.lib.ObjectID(request.params.token)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        let documentsImage = item.medicalDocuments.map(document => {
          return {
            id: document.id,
            img: `http://${this.helper.apiHost}/document/${request.params.token}/${document.img}`,
            status: document.status
          }
        });
        if (documentsImage.length === 0) response.status(412).send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE());
        else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(documentsImage));
      }
    });
  }

  /**
   * Метод для получения информации о документах, отфильтрованных по статусу (verified/unverified)
   * */
  getMedicalDocumentsByStatus(request, response) {
    this.lib.log.trace('Method "getVerifiedMedicalDocuments" - вызван');
    if (!request.params.token || (request.params.status !== 'verified' && request.params.status !== 'unverified')) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getVerifiedMedicalDocuments" - вызван');
    this.lib.db.collection('users').findOne({_id: this.lib.ObjectID(request.params.token)}, (err, item) => {
      if (err) response.status(500).send(this.lib.APIMessage.ERROR.SERVER_ERROR(err));
      else if (item === null) response.status(403).send(this.lib.APIMessage.ERROR.ACCESS_DENIED());
      else {
        let documents = item.medicalDocuments.filter(document => document.status === request.params.status);
        if (documents.length === 0) response.status(202).send(this.lib.APIMessage.SUCCESS.MEDICAL_DOCUMENT_FILTER__EMPTY());
        else response.send(documents);
      }
    });
  }

  getUserSummaryForJob(request, response) {
    this.lib.log.trace('Method "getUserSummaryForJob" - вызван');
    if (!request.params.userId) {
      response.status(400).send(this.lib.APIMessage.ERROR.WRONG_PARAMS());
      return;
    }
    this.lib.log.trace('Method "getUserSummaryForJob" - вызван');
    this.helper.searchUserByID(request.params.userId).then(user => {
      if (!user.jobSummary) response.status(204);
      else response.send(this.lib.APIMessage.SUCCESS.EMPTY_MESSAGE(Object.assign({}, this.helper.getBasicUserInfo(user), user.jobSummary)));
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = User;
