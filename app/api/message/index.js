const APIMessage = require('./message');

class APIMessageSuccess extends APIMessage {
  static EMPTY_MESSAGE(data = []) {
    return this.createSuccessMessage('', data);
  }

  static RESET_PASSWORD_LINK_SUCCESS() {
    return this.createSuccessMessage('Ссылка для восстановления была отправлена на указанную почту.');
  }

  static RESET_PASSWORD_SUCCESS() {
    return this.createSuccessMessage('Пароль успешно изменен! Перенаправление на страницу авторизации через 3 секунды...');
  }

  static REGISTRATION_SUCCESS(registerEmail) {
    return this.createSuccessMessage(
      `Пользователь успешно зарегистрирован! Мы отправили письмо на указанный почтовый ящик (${registerEmail}). Пожалуйста, проверьте вашу почту.`
    );
  }

  static REMIND_CREATED(remindData) {
    return this.createSuccessMessage('Напоминание успешно создано!', remindData);
  }

  static MESSAGE_SEND_SUCCESS() {
    return this.createSuccessMessage('Сообщение успешно отправлено!');
  }

  static FRIEND_REQUEST_SUCCESS() {
    return this.createSuccessMessage('Запрос на дружбу успешно отправлен');
  }

  static FRIEND_REQUEST_EMPTY() {
    return this.createSuccessMessage('У вас нет заявок в друзья');
  }

  static FRIEND_REQUEST_ACCEPT() {
    return this.createSuccessMessage('Вы подтвердили заявку в друзья');
  }

  static FRIENDS_EMPTY() {
    return this.createSuccessMessage('У вас нет друзей');
  }

  static USER_EMPTY() {
    return this.createSuccessMessage('Нет пользователей');
  }

  static NEWS_ADDED() {
    return this.createSuccessMessage('Новость успешно добавлена');
  }

  static NEWS_EMPTY() {
    return this.createSuccessMessage('Нет новостей');
  }

  
  static MONEY_HELP_EMPTY() {
    return this.createSuccessMessage('На данный момент помогать не нужно');
  }

  static ADD_POST_SUCCESS() {
    return this.createSuccessMessage('Пост успешно добавлен');
  }

  static ADD_POST_EMPTY() {
    return this.createSuccessMessage('Нет постов');
  }

  static POST_NOT_EXISTS() {
    return this.createSuccessMessage('Пост не найден!');
  }

  static MEDICAL_DOCUMENT_ALREADY_VERIFIED() {
    return this.createErrorMessage('Данный документ уже был подтвержден');
  }

  static MEDICAL_DOCUMENT_VERIFIED_SUCCESS() {
    return this.createErrorMessage('Документ был успешно подтвержден');
  }

  static MEDICAL_DOCUMENT_FILTER__EMPTY() {
    return this.createErrorMessage('Нет подходящих документов под данный критерий');
  }

  static JOBS_EMPTY() {
    return this.createErrorMessage('Нет вакансий');
  }

  static JOBS_EMPTY_MODERATION() {
    return this.createErrorMessage('Нет вакансий для модерации');
  }

  static JOBS_ADD_SUCCESS() {
    return this.createErrorMessage('Вакансия была отправлена на модерацию. После того, как ее одобрит администратор, она будет видна всем');
}

  static JOBS_ADD_SUCCESS_ADM() {
    return this.createErrorMessage('Вакансия была успешно создана.');
  }

  static ASK_QUESTION_SUCCESS() {
    return this.createSuccessMessage('Воспрос создан! Ожидайте ответа специалиста');
  }

  static QUESTION_CLOSED_SUCCESS() {
    return this.createSuccessMessage('Вопрос был успешно закрыт');
  }
}

class APIMessageError extends APIMessage {
  static WRONG_PARAMS() {
    return this.createErrorMessage('Не все параметры были переданы!');
  }
  
  static WRONG_EMAIL() {
    return this.createErrorMessage('Невозможно создать аккаунт с данным E-mail, так как он не существует');
  }

  static PARAMS_VALIDATE_ERROR() {
    return this.createErrorMessage('Параметры не прошли проверку на валидность!');
  }

  static ACCESS_DENIED() {
    return this.createErrorMessage('Отказано в доступе!');
  }

  static WRONG_ROLE_NAME() {
    return this.createErrorMessage('Неправильная роль пользователя!');
  }

  static WRONG_CREDITIONAL() {
    return this.createErrorMessage('Неправильный логин или пароль!');
  }

  static WRONG_EMAIL() {
    return this.createErrorMessage('Указанный E-mail не был найден в базе данных!');
  }

  static ALREADY_CONFIRM_BY_EMAIL() {
    return this.createErrorMessage(`Вы уже подтвердили свой аккаунт!`);
  }

  static AUTH_NOT_CONFIRMED_EMAIL() {
    return this.createErrorMessage(`Вы не подтвердили свой аккаунт через E-mail!`);
  }

  static USER_NOT_CONFIRMED_EMAIL() {
    return this.createErrorMessage(`Пользователь еще не подтвердил свой E-mail`);
  }

  static USER_DEACTIVATED() {
    return this.createErrorMessage(`Ваш аккаунт был деактивирован. Обратитесь к администратору для решения проблемы`);
  }

  static AUTH_NOT_CONFIRMED_ADMIN(data = []) {
    return this.createErrorMessage(
      `"Гемофилик" является закрытым сообществом. По-этому, мы должны вручную проверить пользователей для того, чтобы дать вам доступ.`,
      data
    );
  }

  static WRONG_ROLE_NAME() {
    return this.createErrorMessage('Неправильное название роли!');
  }

  static WRONG_USER_ID() {
    return this.createErrorMessage('Неправильный userID пользователя');
  }

  static WRONG_DATE() {
    return this.createErrorMessage('Неправильная дата!');
  }

  static USER_EXIST() {
    return this.createErrorMessage('Пользователь был найден в бд!');
  }

  static SERVER_ERROR(errorMessage) {
    return this.createErrorMessage(`Произошла ошибка! ${errorMessage}`);
  }

  static WRONG_DATE_FORMAT() {
    return this.createErrorMessage('Неправильный формат даты!');
  }

  static WRONG_TIME_FORMAT() {
    return this.createErrorMessage('Неправильный формат времени!');
  }

  static DATE_CAN_NOT_BE_LESS_THAN_CURRENT() {
    return this.createErrorMessage('Дата не может быть раньше, чем сегодня');
  }

  static WRONG_REMIND_ID() {
    return this.createErrorMessage('Неправильный идентификатор напоминания!');
  }

  static NO_REMIND() {
    return this.createErrorMessage('На данном момент у вас нет напоминаний');
  }

  static MESSAGE_SEND_FAILED(err) {
    return this.createErrorMessage('Не удалось отправить сообщение :( Текст ошибки: ' + err);
  }

  static USER_NOT_FOUND() {
    return this.createErrorMessage('Не удалось найти пользователя');
  }

  static PRIVACY_NOT_ALLOW() {
    return this.createErrorMessage('Пользователь ограничил данную возможность!');
  }

  static IS_NOT_REQUESTED_FOR_FRIEND() {
    return this.createErrorMessage('Невозможно принять приглашение, так как этот пользователь не добавлял вас в друзья');
  }

  static ALREADY_FRIEND() {
    return this.createErrorMessage('Невозможно принять приглашение, так как этот пользователь уже ваш друг');
  }

  static WRONG_IMAGE_FROMAT() {
    return this.createErrorMessage('Неправильный формат изображения');
  }

  static WRONG_NEWS_ID() {
    return this.createErrorMessage('ID новости не был найден!');
  }

  static ANSWER_WRONG_SPECIALIST(specialistName) {
    return this.createErrorMessage(`Вы не можете отвечать на этот вопрос, так как не являетесь специалистом "${specialistName}"`);
  }

  static ANSWER_WRONG_USER() {
    return this.createErrorMessage(`Вы не можете отвечать на этот вопрос, так как не являетесь создателем этого вопроса`);
  }

  static QUESTION_THEME_NOT_FOUND() {
    return this.createErrorMessage(`Тема не найдена!`);
  }

  static QUESTION_ALREADY_CLOSED() {
    return this.createErrorMessage(`Вы не можете ответить на этот вопрос, так как он был закрыт создателем`);
  }

  static QUESTION_NOT_OWNER() {
    return this.createErrorMessage(`Вы не можете управлять данным вопросом, так как не являетесь создателем этого вопроса`);
  }

  static WRONG_FILE_EXT(ext = false) {
    return this.createErrorMessage(`Неправильное расширение файла!` + (ext ? `Поддерживаются только ${ext}` : ''));
  }

  static WRONG_FILE_SIZE() {
    return this.createErrorMessage(`Превышен максимально допустимый размер файла!`);
  }

  static WRONG_FORUM_THEME_ID() {
    return this.createErrorMessage(`Не правильный id темы!`);
  }

  static WRONG_FORUM_TOPIC_ID() {
    return this.createErrorMessage(`Не правильный id топика!`);
  }

  static MAX_SUB_FORUM_THEMES() {
    return this.createErrorMessage(`Невозможно добавить подраздел к теме, так как достигнута максимальная вложенность - 2`);
  }

  static SUB_FORUM_IS_NOT_GLOBAL_THEME() {
    return this.createErrorMessage(`Невозможно добавить подраздел как глобальную тему`);
  }

  static LIBRARY_ID_NOT_FOUND() {
    return this.createErrorMessage(`ID раздела базы знаний не был найден!`);
  }

  static LIBRARY_BOOK_ID_NOT_FOUND() {
    return this.createErrorMessage(`ID статьи базы знаний не был найден`);
  }

  static CANT_ADD_BOOK_TO_BOOK_LIB() {
    return this.createErrorMessage(`Вы не можете создать статью в статье!`);
  }

  static LIBRARY_FOLDER_NOT_BOOK() {
    return this.createErrorMessage(`Невозможно получить данные о статье, так id является разделом`);
  }

  static CANT_ADD_FOLDER_TO_BOOK_LIB() {
    return this.createErrorMessage(`Вы не можете создать раздел в статье!`);
  }

  static LIBRARY_IMAGE_NEED() {
    return this.createErrorMessage(`Вы не добавили картинку к статье`);
  }

  static THERAPIST_WRONG_ROLE() {
    return this.createErrorMessage(`Пользователь с ролью "специалист-консултант" не может добавлять лечащего врача`);
  }

  static THERAPIST_CANT_ADD_WITHOUT_FRIEND_ACCEPT() {
    return this.createErrorMessage(`Для отправки запроса необходимо, чтобы врач принял запрос в друзья`);
  }

  static THERAPIST_CANT_ADD_SIMPLE_USER() {
    return this.createErrorMessage(`Добавлять в качестве лечащего врача можно только пользователя с ролью "Специалист-консультант"`);
  }

  static THERAPIST_CANT_ADD_SAME_USER() {
    return this.createErrorMessage(`Вы не можете отправить заявку на лечащего врача самому себе`);
  }

  static THERAPIST_CAN_ACCEPT_ONLY_SPECIALIST() {
    return this.createErrorMessage(`Вы не можете принимать заявку на становление лечащим врачом`);
  }

  static THERAPIST_CANT_ADD_USER_THAT_ALREADY_ADDED() {
    return this.createErrorMessage(`Данный пользователь уже является вашим лечащим врачом`);
  }

  static THERAPIST_CANT_FOUND_USER_IN_LIST() {
    return this.createErrorMessage(`Пользователь не был найден в списке ожидающих на подтверждение лечащего врача`);
  }

  static THERAPIST_CANT_SEND_MESSAGE_NO_ACCEPTED() {
    return this.createErrorMessage(`Вы не можете отправлять сообщение этому пользователю`);
  }

  static USER_IS_NOT_SPECIALIST() {
    return this.createErrorMessage(`Вы не обладаете правами специалиста-консультанта`);
  }

  static WRONG_IMAGE() {
    return this.createErrorMessage(`Неправильный формат изображения`);
  }
}

module.exports = {
  SUCCESS: APIMessageSuccess,
  ERROR: APIMessageError,
  CREATOR: APIMessage
};
