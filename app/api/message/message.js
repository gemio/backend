class APIMessage {
  /**
   * Метод для создания сообщения
   * @param {string} type Тип сообщения
   * @param {string} message Текст сообщения
   * @param {array} data Данные
   * */
  static createMessage(type, message, data) {
    return {
      message: {
        type: type,
        message: message
      },
      data: data
    }
  }

  static createSuccessMessage(message, data = []) {
    return this.createMessage('success', message, data);
  }

  static createWarningMessage(message, data = []) {
    return this.createMessage('warning', message, data);
  }

  static createErrorMessage(message, data = []) {
    return this.createMessage('error', message, data);
  }
}

module.exports = APIMessage;