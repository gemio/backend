class Config {
  constructor() {
    const SETTING_PATH = './setting.work';
    this.database = require('./db');
    this.setting = require(SETTING_PATH);
    this.isProduction = SETTING_PATH.includes('gemonet');
    this.log = {
      appenders: {
        out: {
          type: 'stdout',
          layout: {type: 'colored'}
        }
      },
      categories: {
        default: {
          appenders: ['out'],
          level: 'ALL'
        }
      }
    };
    this.validateSetting();
  }

  validateSetting() {
    const logger = require('log4js');
    const log = logger.getLogger('CONFIG');
    logger.configure(this.log);
    let error;

    if (!this.database || !this.setting) error = `Cant't parse settings`;
    if (this.setting.frontend.subFolder[this.setting.frontend.subFolder.length - 1] !== '/') error = 'Wrong subFolder! Should end at "/"';
    if (this.setting.frontend.host.includes('http')) error = 'Frontend.host should not have a protocol!';
    if (this.setting.api.host.includes('http')) error = 'api.host should not have a protocol!';
    if (this.setting.socket.subURL[this.setting.socket.subURL.length] === '/') error = 'Wrong socket.subURL! Example: "/chat"';

    if (error) log.fatal(error);
  }
}

module.exports = new Config();