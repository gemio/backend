// =============== REQUIRE LIBS =================
const express     = require('express');
const app         = express();
const http        = require('http');
const config      = require('./config');
const logger      = require('log4js');
const log         = logger.getLogger('SERVER');
const MongoClient = require('mongodb').MongoClient;
const bodyParser  = require('body-parser');
logger.configure(config.log);
// ==============================================

// ================ DATABASE CONNECT ===============
log.info('Подключение к бд!');
db = MongoClient.connect(config.database.url)
  .then(db => start(db))
  .catch(err => log.fatal(`Возникла ошибка при подключении к бд! ${err}`));
// =========================================

// ================ APP USE =================
app.use(bodyParser.urlencoded({extended: true}));
// ==========================================


// ===================== SERVER ======================
function start(db) {
  let database = db.db('gem');
  log.info('Подключение к бд произошло успешно!');

  http.createServer(app).listen(8081, () => {
    // Require some modules for server
    require('./api')(app, {log: require('log4js').getLogger('API'), db: database});

    log.info('Сервер успешно запущен! Порт: ', config.setting.server.port);
  });
}
// ===================================================