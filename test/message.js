process.env.NODE_ENV = 'test';
const CONFIG = {
  HOST: 'localhost:8080',

  user: {
    test1: {
      email: 'test1@yandex.ru',
      password: 'morozov',
      firstname: 'Олег',
      surname: 'Тест1',
      role: 'Пользователь'
    },

    test2: {
      email: 'test2@yandex.ru',
      password: 'morozov',
      firstname: 'Олег',
      surname: 'Тест2',
      role: 'Пользователь'
    },

    test3: {
      email: 'test3@yandex.ru',
      password: 'morozov',
      firstname: 'Олег',
      surname: 'Тест3',
      role: 'Пользователь'
    }
  },
};

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const queryString = require('query-string');
chai.use(chaiHttp);

// Additional test for create a test env
describe('Creating an user', () => {
  it('Create test1', done => {
    chai.request(CONFIG.HOST)
      .post('/user')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(CONFIG.user.test1)
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        done();
      });
  });

  it('Create test2', done => {
    chai.request(CONFIG.HOST)
      .post('/user')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(CONFIG.user.test2)
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        done();
      });
  });

  it('Create test3', done => {
    chai.request(CONFIG.HOST)
      .post('/user')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(CONFIG.user.test3)
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        done();
      });
  });
});

// Test main func
describe('Testing messages', function() {
  this.timeout(20000);
  let users = [];
  const getFullNameMessage = (user) => {
    return `${user.public.fullName.surname} ${user.public.fullName.name}`
  };

  it('Auth test1', done => {
    chai.request(CONFIG.HOST)
      .get('/user/auth?' + queryString.stringify({
        login: CONFIG.user.test1.email,
        password: CONFIG.user.test1.password
      }))
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        users.push(result.body.data);
        done();
      });
  });

  it('Auth test2', done => {
    chai.request(CONFIG.HOST)
      .get('/user/auth?' + queryString.stringify({
        login: CONFIG.user.test2.email,
        password: CONFIG.user.test2.password
      }))
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        users.push(result.body.data);
        done();
      });
  });

  it('Auth test3', done => {
    chai.request(CONFIG.HOST)
      .get('/user/auth?' + queryString.stringify({
        login: CONFIG.user.test3.email,
        password: CONFIG.user.test3.password
      }))
      .end((err, result) => {
        result.should.have.status(200);
        result.body.should.be.a('object');
        result.body.message.should.have.property('type').eql('success');
        users.push(result.body.data);
        done();
      });
  });

  describe('Empty messages', () => {
    it('test1', done => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          result.should.have.status(412);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('test2', done => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          result.should.have.status(412);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('test3', done => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(412);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });
  });

  describe('test1 send -> test2', () => {
    it('Sending message', done => {
      chai.request(CONFIG.HOST)
        .post('/message')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send({
          fromUser: users[0]._id,
          toUser: users[1].id,
          textMessage: '123'
        })
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('Last message test1', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.include('Вы: 123');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));
          done();
        });
    });

    it('Last message test2', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.equal('123');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));
          done();
        });
    });

    it('Last message test3', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(412);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });
  });

  describe('test2 send -> test1', () => {
    it('Sending message', done => {
      chai.request(CONFIG.HOST)
        .post('/message')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send({
          fromUser: users[1]._id,
          toUser: users[0].id,
          textMessage: '2'
        })
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('Last message test1', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.equal('2');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));
          done();
        });
    });

    it('Last message test2', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.include('Вы: 2');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));
          done();
        });
    });

    it('Last message test3', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(412);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });
  });

  describe('test1 send -> test2', () => {
    it('Sending message', done => {
      chai.request(CONFIG.HOST)
        .post('/message')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send({
          fromUser: users[0]._id,
          toUser: users[1].id,
          textMessage: '3'
        })
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('Last message test1', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.include('Вы: 3');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));
          done();
        });
    });

    it('Last message test2', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.equal('3');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));
          done();
        });
    });

    it('Last message test3', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(412);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });
  });

  describe('test1 send -> test3', () => {
    it('Sending message', done => {
      chai.request(CONFIG.HOST)
        .post('/message')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send({
          fromUser: users[0]._id,
          toUser: users[2].id,
          textMessage: '4'
        })
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('Last message test1', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test2
          result.body.data[0].message.text.should.include('Вы: 3');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));

          // test3
          result.body.data[1].message.text.should.include('Вы: 4');
          result.body.data[1].message.avatar.should.include(users[2].id);
          result.body.data[1].message.userID.should.include(users[2].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[2]));
          done();
        });
    });

    it('Last message test2', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.equal('3');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));
          done();
        });
    });

    it('Last message test3', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(1);
          result.body.data[0].message.text.should.equal('4');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));
          done();
        });
    });
  });

  describe('test2 send -> test3', () => {
    it('Sending message', done => {
      chai.request(CONFIG.HOST)
        .post('/message')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send({
          fromUser: users[1]._id,
          toUser: users[2].id,
          textMessage: '5'
        })
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('Last message test1', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test2
          result.body.data[0].message.text.should.include('Вы: 3');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));

          // test3
          result.body.data[1].message.text.should.include('Вы: 4');
          result.body.data[1].message.avatar.should.include(users[2].id);
          result.body.data[1].message.userID.should.include(users[2].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[2]));
          done();
        });
    });

    it('Last message test2', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test1
          result.body.data[0].message.text.should.equal('3');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));

          // test3
          result.body.data[1].message.text.should.equal('Вы: 5');
          result.body.data[1].message.avatar.should.include(users[2].id);
          result.body.data[1].message.userID.should.include(users[2].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[2]));
          done();
        });
    });

    it('Last message test3', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test1
          result.body.data[0].message.text.should.equal('4');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));

          // test2
          result.body.data[1].message.text.should.equal('5');
          result.body.data[1].message.avatar.should.include(users[1].id);
          result.body.data[1].message.userID.should.include(users[1].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[1]));
          done();
        });
    });
  });

  describe('test3 send -> test2', () => {
    it('Sending message', done => {
      chai.request(CONFIG.HOST)
        .post('/message')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send({
          fromUser: users[2]._id,
          toUser: users[1].id,
          textMessage: '6'
        })
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('Last message test1', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test2
          result.body.data[0].message.text.should.include('Вы: 3');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));

          // test3
          result.body.data[1].message.text.should.include('Вы: 4');
          result.body.data[1].message.avatar.should.include(users[2].id);
          result.body.data[1].message.userID.should.include(users[2].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[2]));
          done();
        });
    });

    it('Last message test2', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test1
          result.body.data[0].message.text.should.equal('3');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));

          // test3
          result.body.data[1].message.text.should.equal('6');
          result.body.data[1].message.avatar.should.include(users[2].id);
          result.body.data[1].message.userID.should.include(users[2].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[2]));
          done();
        });
    });

    it('Last message test3', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test1
          result.body.data[1].message.text.should.equal('4');
          result.body.data[1].message.avatar.should.include(users[0].id);
          result.body.data[1].message.userID.should.include(users[0].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[0]));

          // test2
          result.body.data[0].message.text.should.equal('Вы: 6');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));
          done();
        });
    });
  });

  describe('test3 send -> test1', () => {
    it('Sending message', done => {
      chai.request(CONFIG.HOST)
        .post('/message')
        .set('content-type', 'application/x-www-form-urlencoded')
        .send({
          fromUser: users[2]._id,
          toUser: users[0].id,
          textMessage: '7'
        })
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          done();
        });
    });

    it('Last message test1', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[0]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test3
          result.body.data[1].message.text.should.include('7');
          result.body.data[1].message.avatar.should.include(users[2].id);
          result.body.data[1].message.userID.should.include(users[2].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[2]));

          // test2
          result.body.data[0].message.text.should.include('Вы: 3');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));
          done();
        });
    });

    it('Last message test2', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[1]._id}`)
        .end((err, result) => {
          if (err) done(err);
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test1
          result.body.data[0].message.text.should.equal('3');
          result.body.data[0].message.avatar.should.include(users[0].id);
          result.body.data[0].message.userID.should.include(users[0].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[0]));

          // test3
          result.body.data[1].message.text.should.equal('6');
          result.body.data[1].message.avatar.should.include(users[2].id);
          result.body.data[1].message.userID.should.include(users[2].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[2]));
          done();
        });
    });

    it('Last message test3', (done) => {
      chai.request(CONFIG.HOST)
        .get(`/message/last/${users[2]._id}`)
        .end((err, result) => {
          result.should.have.status(200);
          result.body.should.be.a('object');
          result.body.message.should.have.property('type').eql('success');
          result.body.data.should.have.lengthOf(2);

          // test1
          result.body.data[1].message.text.should.equal('Вы: 7');
          result.body.data[1].message.avatar.should.include(users[0].id);
          result.body.data[1].message.userID.should.include(users[0].id);
          result.body.data[1].message.toUser.should.equal(getFullNameMessage(users[0]));

          // test2
          result.body.data[0].message.text.should.equal('Вы: 6');
          result.body.data[0].message.avatar.should.include(users[1].id);
          result.body.data[0].message.userID.should.include(users[1].id);
          result.body.data[0].message.toUser.should.equal(getFullNameMessage(users[1]));
          done();
        });
    });
  });
});